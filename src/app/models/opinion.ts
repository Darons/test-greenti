export class Opinion {

	constructor() {
		this.description = '';
		this.id = -1;
	}
	id: number;
	description: string;
	restaurant_id: number;
}
