import { Component, OnInit } from '@angular/core';
import { RestaurantService } from './services/restaurant.service';
import { Restaurant } from './models/restaurant';
import { Opinion } from './models/opinion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	
	restaurants: Array < Restaurant > = [];

	constructor(private rs: RestaurantService) {}

	ngOnInit(): void {
		//Called after the constructor, initializing input properties, and the first call to ngOnChanges.
		//Add 'implements OnInit' to the class.
		this.rs.getRestaurants().subscribe(res => this.restaurants = res);
	}

	addOp(i: number): void {
		this.restaurants[i].opinions.push(new Opinion());
	}

	addRes(): void {
		this.restaurants.push(new Restaurant());
	}

	save(): void {
		let i = 0;
		let body = {};
		this.restaurants.forEach(res => {
			res.opinions_attributes = res.opinions;
			body[i] = res;
			i++;
		});
		this.rs.saveRestaurants(body).subscribe(() => {
			this.rs.getRestaurants().subscribe(res => this.restaurants = res);
		})
	}
}
